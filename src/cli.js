#!/usr/bin/env node

import fs from 'node:fs'
import path from 'node:path'
import util from 'node:util'
import os from 'node:os'
import {execSync} from 'node:child_process'

const json = (x) => JSON.stringify(x)
const jsonpp = (x) => JSON.stringify(x, null, 2)
const unjson = (x) => JSON.parse(x)
const sleep = (ms) => new Promise(res => setTimeout(res, ms))
const elapsed = (start, timeout) => (Date.now() >= (+start) + timeout)
const qw = (x) => (Array.isArray(x) ? x : (x.match(/\S+/g) ?? [ ]))
const pp = (...vs) => console.log(...vs)


start()


//------------------------------------------------------------------------------------------------------
// functions only

async function start() {
    try {
        const config_filename = path.resolve(os.homedir(), '.gits.config.json')
        const config = unjson(fs.readFileSync(config_filename, 'utf8'))

        const ctx = {
            root_dirs: config.root_dirs.map(path.normalize),
            ignore_dirs: config.ignore_dirs.map(path.normalize),
            project_files: ['.git', ...config.project_files],
            results: [ ],
        }

        for (const dir of ctx.root_dirs) {
            search(ctx, path.resolve(dir))
        }

        for (const result of ctx.results) {
            if (result.found_names.includes('.git')) {
                const has_remote = ('' !== run(result.dir, 'git remote show'))
                const has_changes = ('' !== run(result.dir, 'git status -s'))
                const is_ahead = has_remote && ('' !== run(result.dir, 'git log --branches --not --remotes --oneline'))
                const has_stash = ('' !== run(result.dir, 'git stash list'))

                if (has_changes || is_ahead || has_stash) {
                    const status = (
                        (has_changes ? 'C' : ' ') +
                        (!has_remote ? '-' : is_ahead ? 'P' : ' ') +
                        (has_stash ? 'S' : ' ')
                    )
                    pp(status, result.dir)
                }
                else {
                    // project is clean
                }
            }
            else {
                // no git, so remote/changes/ahead/stash is not applicable
                pp('-- ', result.dir)
            }
        }
    }
    catch (error) {
        console.error(error)
        process.exit(1)
    }
}

function run(cwd, command) {
    const stdout = execSync(command, {cwd}).toString()
    return stdout.replace(/[\r\n]+$/, '')
}

function search(ctx, dir) {
    if (ctx.ignore_dirs?.includes(dir) || ctx.ignore_dirs?.includes(path.basename(dir))) {
        return
    }

    const dirents = fs.readdirSync(dir, {withFileTypes:true})
    const found_names = [ ]

    for (const dirent of dirents) {
        if (ctx.project_files.includes(dirent.name)) {
            found_names.push(dirent.name)
        }
    }

    if (found_names.length > 0) {
        ctx.results.push({dir, found_names})
        return // do not descend into a project directory
    }

    for (const dirent of dirents) {
        if (dirent.isDirectory()) {
            search(ctx, path.resolve(dir, dirent.name))
        }
    }
}
