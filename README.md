
# Gits-status

Show git status for all projects.


## Install & run

```
npm link
yarn link
gits
```

TODO: publish into NPM.


## Status characters

```
CPS /path/to/project

C: has changes?
    'C' yes
    ' ' no changes
    '-' no .git directory

P: has something to push (is ahead of origin)?
    'P' yes
    ' ' nothing to push
    '-' no origin

S: has stashed changes?
    'S' yes
    ' ' no stashes
```

Projects that are "versioned and clean" are not displayed (i.e. have a `.git` directory,
but no changes, no stashes and nothing to push).


## Config file

A config file is required at `$HOME/.gits.config.json`:

```json
{
    "root_dirs": [
        "c:/CmdTools",
        "c:/Vim",
        "c:\\Users\\User\\Documents\\Projects"
    ],

    "ignore_dirs": [
        "node_modules",
        ".svn",
        "c:/Users/User/Documents/Projects/Sort",
        "c:/Vim/vimfiles/pack/plugins"
    ],

    "project_files": [
        "package.json"
    ]
}
```

### `root_dirs`

A list of directories to search for projects in. Only absolute paths.

### `ignore_dirs`

A list of directories to never recurse into. Names or absolute paths.

Use it to speed up search by avoiding big directories like `node_modules`,
or to clear the output from archived or unwanted projects.

If a directory appears to be a project, it is automatically ignored.
`gits` doesn't search for projects in other projects' directories.

### `project_files`

A list of file or directory names that project directories may contain.
`.git` is always implied.
